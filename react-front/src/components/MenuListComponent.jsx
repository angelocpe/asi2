import { MenuItemComponent } from "./MenuItemComponent";
import "./CSS/MenuListComponent.css"
import { useNavigate } from "react-router-dom";
import pages from "../pages/pages";

export function MenuListComponent (){

    const navigate = useNavigate();

    return(
        <div id="menuList">
            <div class="d-flex flex-row">
                <div class="menuItem" onClick={() => navigate(`/${pages.router.CardsBuy}`)}>
                    <MenuItemComponent icon= "https://cdn-icons-png.flaticon.com/512/2717/2717989.png " label={pages.title.buy}/>
                </div>
                <div class="menuItem" onClick={() => navigate(`/${pages.router.CardsSell}`)}>
                    <MenuItemComponent icon= "https://cdn-icons-png.flaticon.com/512/4285/4285874.png" label={pages.title.sell}/>
                </div>
            </div>

            <div class="d-flex flex-row">
                <div class="menuItem" onClick={() => navigate(`/${pages.router.Play}`)}>
                    <MenuItemComponent icon= "https://cdn-icons-png.flaticon.com/512/4738/4738879.png" label={pages.title.play}/>
                </div>
                <div class="menuItem" onClick={() => navigate(`/${pages.router.Chat}`)}>
                    <MenuItemComponent icon= "https://cdn-icons-png.flaticon.com/512/1370/1370907.png " label={pages.title.chat}/>
                </div>
            </div>            
        </div>
    );
}