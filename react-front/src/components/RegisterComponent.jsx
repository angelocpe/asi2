import Button from 'react-bootstrap/Button';
import './CSS/LoginComponent.css';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import UserDTO from '../DTOs/UserDTO';
import UserService from '../services/user.service';
import { signout } from '../core/actions/user.actions';


export function RegisterComponent (){
    
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const [nameUser, setNameUser] = useState('');
    const [surnameUser, setSurnameUser] = useState('');
    const [pwdUser, setPwdUser] = useState('');
    const [rePwdUser, setRePwdser] = useState('');

    const handleChangeName = (e) => {
        setNameUser(e.target.value)
    }

    const handleChangeSurname = (e) => {
        setSurnameUser(e.target.value)
    }

    const handleChangePwd = (e) => {
        setPwdUser(e.target.value)
    }

    const handleChangeRePwd = (e) => {
        setRePwdser(e.target.value)
    }

    async function userSubmit() {

        const newUser = new UserDTO({
            id: 0,
            login: nameUser,
            pwd: pwdUser,
            account: 5000,
            lastName: surnameUser,
            surName: surnameUser,
            email: "",
            cardList: [],
        });

        const userResponse = await UserService.postUser(newUser);

        // Success
        if(userResponse) {
            dispatch(signout());
            navigate("/");
        // Error happened
        } else {
            alert("Identifiants incorrects");
        }
    };

    return(
        <div className="form">
            <form>
                <div>
                    <input placeholder= "Name" type="text" name = "name" value={nameUser} onChange={handleChangeName} />
                    <input placeholder = "Surname" type="text" name = "surname" value={surnameUser} onChange={handleChangeSurname} />
                    <input placeholder = "Password" type="password" name = "password" value={pwdUser} onChange={handleChangePwd} />
                    <input placeholder = "Re-Password" type="password" value={rePwdUser} onChange={handleChangeRePwd} />
                </div>
                <div>
                    <Button variant="dark" onClick={() => userSubmit() } style={{width: '100%'}}>
                        Envoyer
                    </Button>
                    <Button variant="dark" onClick={() => navigate('/')} style={{width: '100%'}}>
                        Cancel
                    </Button>
                </div>
            </form>
        </div>
    );
}