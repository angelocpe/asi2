import Table from 'react-bootstrap/Table';
import React from 'react';
import { useDispatch } from 'react-redux';
import { setCurrentCard, setCurrentCardId } from '../core/actions/card.actions';

export function CardGridComponent(props) {

  const dispatch = useDispatch();
  let rows = []
  props.cards.forEach(element => 
    rows.push(          
      <tr key={element.id} onClick={()=> {
          dispatch(setCurrentCard(element));
          dispatch(setCurrentCardId(element.id));
        }}>
          <td>{element.name}</td>
          <td>{element.description}</td>
          <td>{element.family}</td>
          <td>{element.affinity}</td>
          <td>{element.energy}</td>
          <td>{element.hp}</td>
          <td>{element.price}</td>
      </tr> 
    ));

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Family</th>
          <th>Affinity</th>
          <th>Energy</th>
          <th>HP</th>
          <th>price</th>
        </tr>
      </thead>
      <tbody>
        {rows}
      </tbody>
    </Table>
  );
}

