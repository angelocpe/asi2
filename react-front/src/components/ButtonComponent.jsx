export function ButtonComponent(props) {
    const onClickFunction = () => props.actionFunction();

    return(
        <button onClick={onClickFunction} >
            {props.text}
        </button>
    )
}