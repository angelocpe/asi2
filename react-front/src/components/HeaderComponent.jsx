import { AmountComponent } from './AmountComponent';
import { ActionLabelComponent } from './ActionLabelComponent';
import { UserInfoComponent } from './UserInfoComponent';
import './CSS/HeaderComponent.css'
import pages from '../pages/pages';

export function HeaderComponent({account, surname} ) {
    return(
        <div id="header_menu">
            <AmountComponent account= {account} />
            <ActionLabelComponent title={pages.title.home} link={`/${pages.router.Home}`}/>
            <UserInfoComponent surname={surname} />
        </div>
    );
}