import Image from 'react-bootstrap/Image';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { signout } from '../core/actions/user.actions';
import './CSS/UserinfoComponent.css'

export function UserInfoComponent(props){
    const dispatch = useDispatch();
    const navigate = useNavigate();
    return(
        <div id="userinfos">
            <Image src="https://cdn-icons-png.flaticon.com/512/552/552848.png" onClick={() => {
                dispatch(signout());
                navigate('/');
            }}/>
            <div>{props.surname}</div>
        </div>
    );
}