import Button from 'react-bootstrap/Button';
import './CSS/LoginComponent.css';
import { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import UserService from '../services/user.service';
import AuthDTO from '../DTOs/AuthDTO';
import { setCurrentUser, setCurrentUserId, signout } from '../core/actions/user.actions';
import { setUserCards } from '../core/actions/card.actions';

export function LoginComponent (){

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [loginUser, setLoginUser] = useState('');
    const [pwdUser, setPwdUser] = useState('');

    const handleChangeLogin = (e) => {
        setLoginUser(e.target.value)
    }

    const handleChangePwd = (e) => {
        setPwdUser(e.target.value)
    }

    async function userSubmit() {
        const authDTO = new AuthDTO({ username: loginUser, password: pwdUser});
        const authResponse = await UserService.authenticateUser(authDTO);

        // Success
        if(authResponse) {
            const userDTO = await UserService.getUserById(authResponse);
            if(userDTO) {
                dispatch(signout());
                dispatch(setCurrentUserId(authResponse));
                dispatch(setCurrentUser(userDTO));
                dispatch(setUserCards(userDTO.cardList));
            }
        // Error
        } else {
            alert("Identifiants incorrects");
        }
  };

  return(
      <div className="form">
          <form>
              <div>
                  <input placeholder = "Surname" type="text" name = "surname" value={loginUser} onChange={handleChangeLogin} />
                  <input placeholder = "Password" type="password" name = "password" value={pwdUser} onChange={handleChangePwd} />
              </div>
              <div>
                  <Button variant="dark" onClick={() => userSubmit()} style={{width: '100%'}}>
                      Envoyer
                  </Button>
                  <Button variant="dark" onClick={() => navigate('/') } style={{width: '100%'}}>
                      Cancel
                  </Button>
              </div>
          </form>
          <Button variant="dark" type="submit" onClick={() => navigate('/Inscription') } style={{width: '100%'}}>
              Inscription
          </Button>
      </div>
  );
}