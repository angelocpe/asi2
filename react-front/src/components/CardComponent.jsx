import { Card } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { selectCurrentCard } from '../core/selectors/card.selectors';
import { ButtonComponent } from "./ButtonComponent";

export function CardComponent(props ) {
    const currentCard = useSelector(selectCurrentCard);

    if(currentCard) {
        return (
            <div>
                <Card>
                    <Card.Body>
                        <Card.Title>{currentCard.energy} {currentCard.name} {currentCard.hp}</Card.Title>
                        <Card.Img src={currentCard.imgUrl}></Card.Img>
                        <Card.Text>
                            {currentCard.description} {currentCard.family} {currentCard.affinity}
                        </Card.Text>
                    </Card.Body>
                </Card>
                <ButtonComponent text= {props.text} actionFunction={props.actionFunction}/>
             </div>
        );
    } else return;
}

