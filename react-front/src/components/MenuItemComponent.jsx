import Image from 'react-bootstrap/Image';

export function MenuItemComponent (props){
    
    return(
        <div>
            <Image src={props.icon} />
            <div>{props.label}</div>
        </div>
    );
}