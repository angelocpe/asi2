export function InputComponent(props) {
    return(
      <div className="input-container">
        <label>{props.label} </label>
        <input type={props.type} name={props.name} value={props.value} required />
      </div>
      );

} 