import { Container, Row, Col } from "react-bootstrap";
import ListGroup from 'react-bootstrap/ListGroup';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux'
import { SocketContext } from '../../core/socket/socket';
import { useEffect, useContext, useState } from 'react';
import { selectCurrentChatMessages } from '../../core/selectors/chat.selectors';
import { selectAllUsers, selectCurrentUser, selectCurrentUserId, selectOtherUser, selectOtherUserId } from '../../core/selectors/user.selectors';
import { setCurrentChatId, setCurrentMessages } from '../../core/actions/chat.actions';
import SocketService from "../../services/socket.service";
import MessageService from '../../services/message.service';
import environment from "../../environment/environment";


function ChatMessages() {
    const dispatch = useDispatch();
    const socket = useContext(SocketContext);
    const [messageText, setMessageText] = useState('');
    const currentUserId = useSelector(selectCurrentUserId);
    const currentUser = useSelector(selectCurrentUser);
    const otherUser = useSelector(selectOtherUser);
    const otherUserId = useSelector(selectOtherUserId);
    const usersChatMessages = useSelector(selectCurrentChatMessages);
    const users = useSelector(selectAllUsers);

    useEffect(() => {
        const intervalId = setInterval(getAllChatMessages, environment.api_set_interval_time);
        return () => clearInterval(intervalId);
    }, [])


    async function getAllChatMessages() {
        let chatResponse;
        if(otherUserId !== null) chatResponse = await MessageService.getAllUsersMessages(currentUserId, otherUserId);
        else chatResponse = await MessageService.getGlobalChatMessages();
        dispatch(setCurrentMessages(chatResponse));
    }

    const handleChangeText = (e) => {
        setMessageText(e.target.value);
    }

    function onSendMessage() {

        const messageOut = {
            message_id: 0,
            sender_id: currentUser.id,
            receiver_id: otherUserId,
            message: messageText,
            timestamp: new Date().getTime()
        };

        SocketService.emitSendMessageIn(socket, messageOut)
        setMessageText('');
    };
    
    function getUserNameById(id) {
        if(id === currentUserId) {
            return currentUser.login;
        } else {
            if(otherUser) return otherUser.login;
            else {
                for(const user of users) {
                    if(id === user.id) {
                        return user.login;
                    }
                }
                return ''
            }
        }
    }

    return (
        <Container fluid className="vh-100">
            <Col>
                <ListGroup variant='flush'>
                    {usersChatMessages.map(chatMess => <ListGroup.Item key={chatMess.timestamp}>{getUserNameById(chatMess.sender_id)}: {chatMess.message}</ListGroup.Item>)}
                </ListGroup> 
            </Col>
            <Row>
                <Col>
                    <Form.Control 
                        value={messageText} 
                        onChange={handleChangeText} 
                        type="text" 
                        placeholder="Entrer votre message"
                        onKeyPress={event => {
                            if (event.key === 'Enter') {
                                onSendMessage();
                            }
                        }} />
                </Col>
                <Col xs={2}>                
                    <Button variant="dark" type="submit" onClick={() => onSendMessage()}style={{width: '100%'}}>
                        Envoyer
                    </Button>
                </Col>
            </Row>
        </Container>
    );
  }
  
  export default ChatMessages;