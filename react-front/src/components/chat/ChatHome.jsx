import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';

import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { selectCurrentUserId, selectAllOtherUsersIds, selectOtherUserId, selectAllOtherUsers, selectCurrentUser, selectOtherUser } from '../../core/selectors/user.selectors';
import { setOtherUser, setOtherUserId } from '../../core/actions/user.actions';
import { useEffect, useContext, useState } from 'react';
import { SocketContext } from '../../core/socket/socket';
import ChatMessages from './ChatMessages';
import SocketService from "../../services/socket.service";
import UserService from '../../services/user.service';
import environment from '../../environment/environment';
import { setAllUsers } from '../../core/actions/user.actions';
import { setCurrentMessages } from '../../core/actions/chat.actions';

function ChatHome() {
    const dispatch = useDispatch();
    const otherUser = useSelector(selectOtherUser);
    const currentUser = useSelector(selectCurrentUser);
    const otherUsersIds = useSelector(selectAllOtherUsersIds);
    const userId = useSelector(selectCurrentUserId);
    const allOtherUsers = useSelector(selectAllOtherUsers);
    const [showChat, setShowChat] = useState(false);

    const socket = useContext(SocketContext);

    useEffect(() => {
        if(userId) {
            SocketService.bindAllSocketEvents(socket);
            const loginDto = {'user_id': userId};
            SocketService.emitLoginIn(socket, loginDto);
        }
        setInterval(getAllUsers, environment.api_set_interval_time);
    }, [])

    async function getAllUsers() {
        const usersResponse = await UserService.getAllUsers();
        dispatch(setAllUsers(usersResponse));
    }

    const isListEmpty = otherUsersIds.length === 0;

    const otherUserName = otherUser === null ? "tous" : otherUser.login

    function onOpenGlobalChat() {
        setShowChat(true);
        dispatch(setCurrentMessages([]));
        dispatch(setOtherUser(null));
        dispatch(setOtherUserId(null));
    }

    function onOpenChat(otherUser) {
        setShowChat(true);
        dispatch(setCurrentMessages([]));
        dispatch(setOtherUser(otherUser));
        dispatch(setOtherUserId(otherUser.id));
    };

    function onCloseChat() {
        setShowChat(false);
        dispatch(setCurrentMessages([]));
        dispatch(setOtherUserId(null));
    };

    return (
        <div>
            <h1>Vous êtes user { currentUser.login }</h1>
            <Modal show={showChat} fullscreen onHide={() => {onCloseChat()}}>
                <Modal.Header closeButton>
                    <Modal.Title>Discussion avec { otherUserName }</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ChatMessages></ChatMessages>
                </Modal.Body>
            </Modal>
            <Card style={{width:'48rem'}}>
            <ListGroup variant='flush'>
            <ListGroup.Item key={-1} onClick={() => onOpenGlobalChat()}>Chat Global</ListGroup.Item> 
                { isListEmpty ? '' : allOtherUsers.map(other => <ListGroup.Item key={other.id} onClick={() => onOpenChat(other)}>Chat avec l'utilisateur {other.login} </ListGroup.Item>) }
            </ListGroup> 
            </Card>
        </div>    
    );
  }
  
  export default ChatHome;