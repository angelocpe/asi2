import { CardComponent } from "./CardComponent";

export function CardViewComponent(props){

    return(
        <>  
        <CardComponent actionFunction={props.actionFunction} text ={props.text} />
        </>
    );
}