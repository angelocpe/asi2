import { useNavigate } from "react-router-dom";

export function ActionLabelComponent(props){

    const navigate = useNavigate();

    return(  
        <h1 onClick={()=> navigate(props.link)}>
            {props.title}
        </h1>
    );
}