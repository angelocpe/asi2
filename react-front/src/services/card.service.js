import http from "./http";
import environment from "../environment/environment"
import CardDTO from '../DTOs/CardDTO';

const cards_url_start = environment.api_urls.card;
const get_all_cards_url = cards_url_start + '/cards';
const get_card_by_id = cards_url_start + '/card';
const get_user_cards_by_user_id = cards_url_start + '/user_cards';
const get_cards_to_sell = cards_url_start + '/cards_to_sell';

class CardService {
    constructor({}) {
        console.log(`new CardService`);
    }

    // Get all the cards in card-ms
    getAllCards() {
        return http.get(get_all_cards_url)
            .then(cardsDTOs => {
                return cardsDTOs.map(cardDTO => new CardDTO(cardDTO))
            })
            .catch(error => {
                console.error('getAllCards error', error);
                return null;
            });
    }

    // Get cards by user_id
    getUserCardsByUserId(user_id) {
        return http.get(`${get_user_cards_by_user_id}/${user_id}`)
        .then(cardsDTOs => {
            return cardsDTOs.map(cardDTO => new CardDTO(cardDTO))
        })
        .catch(error => {
            console.error('getUserCardsByUserId error', error);
            return null;
        });
    }

    // Get card by id
    getCardById(card_id) {
        return http.get(`${get_card_by_id}/${card_id}`)
            .then(cardDTO => { return new CardDTO(cardDTO) })
            .catch(error => {
                console.error(`getCardById(${card_id}) error:`, error);
                return null;
            });
    }

    // Get all the cards to sell in card-ms
    getAllCardsToSell() {
        return http.get(get_cards_to_sell)
            .then(cardsDTOs => {
                return cardsDTOs.map(cardDTO => new CardDTO(cardDTO))
            })
            .catch(error => {
                console.error('getAllCardsToSell error', error);
                return null;
            });
    }
}

// exports an instance of te CardService class making it a singleton
export default new CardService({});