import SocketEvents from "../core/socket/socket";
import { setOtherConnectedUsersIds, setAllOtherUsersIds } from "../core/actions/user.actions";
import { addOneMessage } from "../core/actions/chat.actions";
import { selectCurrentUserId } from '../core/selectors/user.selectors';
import { setCurrentMessages } from '../core/actions/chat.actions';
import store from "../core/store/store";

class SocketService {
    constructor({}) {
        console.log('new SocketService');
    }

    /*
        Emitting events
    */

    emitLoginIn(socket, loginIn) {
        console.log('login_in', loginIn);
        socket.emit(SocketEvents.login_in, JSON.stringify(loginIn));
    }

    emitSendMessageIn(socket, emitSendMessageIn) {
        console.log('send_message_in', emitSendMessageIn);
        socket.emit(SocketEvents.send_message_in, JSON.stringify(emitSendMessageIn));
    }

    /*
        On Binds event
    */

    // Binds sockets to all defined events
    bindAllSocketEvents(socket) {
        this.bindSocketOnAllUserIdsOut(socket);
        this.bindSocketOnLoggedInUserIdsOut(socket);
        this.bindSocketOnLogginOut(socket);
        this.bindSocketMessageOut(socket);
    }

    bindSocketOnLoggedInUserIdsOut(socket) {
        socket.on(SocketEvents.logged_in_users_ids_out, function(data) {
            console.log('logged_in_users_ids_out', JSON.parse(data));
            const userId = selectCurrentUserId(store.getState());
            store.dispatch(setOtherConnectedUsersIds(JSON.parse(data).filter(id => id !== userId)));
        });
    }

    bindSocketOnAllUserIdsOut(socket) {
        socket.on(SocketEvents.all_users_ids_out, function(data) {
            console.log('all_users_ids_out', JSON.parse(data));
            const userId = selectCurrentUserId(store.getState());
            store.dispatch(setAllOtherUsersIds(JSON.parse(data).filter(id => id !== userId)));
        });
    }

    bindSocketOnLogginOut(socket) {
        socket.on(SocketEvents.login_out, function(data) {
            console.log('login_out', JSON.parse(data));
            store.dispatch(setCurrentMessages(JSON.parse(data)));
        });
    }

    bindSocketMessageOut(socket) {
        socket.on(SocketEvents.send_message_out, function(data) {
            console.log('send_message_out', JSON.parse(data));
            const chatMessage = JSON.parse(data);
            store.dispatch(addOneMessage(chatMessage));
        });
    }
}

export default new SocketService({});