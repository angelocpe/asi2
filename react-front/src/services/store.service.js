import http from "./http";
import environment from "../environment/environment"
import StoreOrderDTO from '../DTOs/StoreOrderDTO';
import TransactionDTO from "../DTOs/TransactionDTO";

const store_url_start = environment.api_urls.store + '/store';
const get_all_transactions_url = store_url_start + '/transaction';
const post_buy_url = store_url_start + '/buy';
const post_sell_url = store_url_start + '/sell';

class StoreService {
    constructor({}) {
        console.log(`new StoreService`);
    }

    // Get all the transactions in store-ms
    getAllTransactions() {
        return http.get(get_all_transactions_url)
            .then(transactionsDTOs => {
                return transactionsDTOs.map(transactionDTO => new TransactionDTO(transactionDTO))
            })
            .catch(error => {
                console.error('getAllTransactions error', error);
                return null;
            });
    }

    // Post to buy
    postBuyStoreOrder(storeOrderDTO) {
        return http.post(post_buy_url, storeOrderDTO)
            .then(storeOrderDTO => { return new StoreOrderDTO(storeOrderDTO) })
            .catch(error => {
                console.error(`postBuyStoreOrder(${storeOrderDTO}) error:`, error);
                return null;
            });
    }

    // Post to buy
    postSellStoreOrder(storeOrderDTO) {
        return http.post(post_sell_url, storeOrderDTO)
            .then(storeOrderDTO => { return new StoreOrderDTO(storeOrderDTO) })
            .catch(error => {
                console.error(`postSellStoreOrder(${storeOrderDTO}) error:`, error);
                return null;
            });
    }
}

// exports an instance of te StoreService class making it a singleton
export default new StoreService({});