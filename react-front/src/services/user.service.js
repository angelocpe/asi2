import http from "./http";
import environment from "../environment/environment"
import UserDTO from '../DTOs/UserDTO';

const users_url_start = environment.api_urls.user;
const get_all_users_url = users_url_start + '/users';
const get_user_by_id = users_url_start + '/user';
const post_user = get_user_by_id;
const put_user_by_id = get_user_by_id;
const post_auth = users_url_start + '/auth';

class UserService {
    constructor({}) {
        console.log(`new UserService`);
    }

    // Get all the users in user-ms
    getAllUsers() {
        return http.get(get_all_users_url)
            .then(usersDTOs => {
                return usersDTOs.map(userDTO => new UserDTO(userDTO))
            })
            .catch(error => {
                console.error('getAllUsers error', error);
                return null;
            });
    }

    // Get user by id
    getUserById(user_id) {
        return http.get(`${get_user_by_id}/${user_id}`)
            .then(userDTO => { return new UserDTO(userDTO) })
            .catch(error => {
                console.error(`getUserById(${user_id}) error:`, error);
                return null;
            });
    }

    // Post a user
    postUser(userDTO) {
        return http.post(post_user, userDTO)
            .then(userDTO => { return new UserDTO(userDTO) })
            .catch(error => {
                console.error(`postUser(${userDTO}) error:`, error);
                return null;
            });
    }

    // Put a user
    putUser(userDTO) {
        return http.put(`${put_user_by_id}/${userDTO.user_id}`, userDTO)
            .then(userDTO => { return new UserDTO(userDTO) })
            .catch(error => {
                console.error(`putUser(${userDTO}) error:`, error);
                return null;
            });
    }

    // Authenticate user
    authenticateUser(authDTO) {
        return http.post(post_auth, authDTO)
            .then(userId => { return userId })
            .catch(error => {
                console.error(`authenticateUser(${authDTO}) error:`, error);
                return null;
            });
    }
}

// exports an instance of te UserService class making it a singleton
export default new UserService({});