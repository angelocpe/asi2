class http {
    constructor({}) {
        console.log(`new http`);
    }

    // Sends a http get method to url and return json object
    get(api_url) {
        return new Promise((resolve, reject) => {
            return fetch(api_url, {
                    method: "GET",
                    'Accept': 'application/json',
                    headers: {'Content-Type': 'application/json'}
                })
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.json());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }

    // Sends a http post method to url and return json object
    post(api_url, data) {
        return new Promise((resolve, reject) => {
            return fetch(api_url, {
                    method: "POST",
                    'Accept': 'application/json',
                    headers: {'Content-Type': 'application/json'}, 
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.json());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }

    // Sends a http put method to url and return json object
    put(api_url, data) {
        return new Promise((resolve, reject) => {
            return fetch(api_url, {
                    method: "PUT",
                    'Accept': 'application/json',
                    headers: {'Content-Type': 'application/json'}, 
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.json());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }
}

export default new http({});