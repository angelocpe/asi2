import http from "./http";
import environment from "../environment/environment"
import MessageDTO from '../DTOs/MessageDTO';

const messages_url_start = environment.api_urls.messages;
const get_all_messages_url = messages_url_start + '/messages';
const get_all_global_messages_url = messages_url_start + '/globalmessages';

class MessageService {
    constructor({}) {
        console.log(`new MessageService`);
    }

    getAllUsersMessages(user_1_id, user_2_id) {
        return http.get(`${get_all_messages_url}?user1id=${user_1_id}&user2id=${user_2_id}`)
            .then(messagesDTOs => {
                return messagesDTOs.map(messageDTO => new MessageDTO(messageDTO))
            })
            .catch(error => {
                console.error('getAllMessages error', error);
                return null;
            });
    }

    getGlobalChatMessages() {
        return http.get(get_all_global_messages_url)
            .then(messagesDTOs => {
                return messagesDTOs.map(messageDTO => new MessageDTO(messageDTO))
            })
            .catch(error => {
                console.error('getAllMessages error', error);
                return null;
            });
    }
}

// exports an instance of te MessageService class making it a singleton
export default new MessageService({});