class UserDTO {
    constructor({ id, login, pwd, account, lastName, surName, email, cardList }) {
        this.id = id;
        this.login = login;
        this.pwd = pwd;
        this.account = account;
        this.lastName = lastName;
        this.surName = surName;
        this.email = email;
        this.cardList = cardList;
    }
}

export default UserDTO;