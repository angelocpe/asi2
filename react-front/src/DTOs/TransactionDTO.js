class TransactionDTO {
    constructor({ id, userId, cardId, userState, cardState, action, timeSt }) {
        this.id = id;
        this.userId = userId;
        this.cardId = cardId;
        this.userState = userState;
        this.cardState = cardState;
        this.action = action;
        this.timeSt = timeSt;
    }
}

export default TransactionDTO;