class StoreOrderDTO {
    constructor({ user_id, card_id }) {
        this.user_id = user_id;
        this.card_id = card_id;
    }
}

export default StoreOrderDTO;