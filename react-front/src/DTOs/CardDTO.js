class CardDTO {
    constructor({ id, energy, hp, defence, attack, price, userId, storeId, name, description, family, affinity, imgUrl, smallImgUrl }) {
        this.id = id;
        this.energy = energy;
        this.hp = hp;
        this.defence = defence;
        this.attack = attack;
        this.price = price;
        this.userId = userId;
        this.storeId = storeId;
        this.name = name;
        this.description = description;
        this.family = family;
        this.affinity = affinity;
        this.imgUrl = imgUrl;
        this.smallImgUrl = smallImgUrl;

    }
}

export default CardDTO;