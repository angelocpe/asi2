import React from 'react';
import { HeaderComponent } from '../components/HeaderComponent';
import { RegisterComponent } from '../components/RegisterComponent';

function Inscription() {

  return (
      <div className="App">
        <header className="App-header">
            <HeaderComponent account="" surname= ""/>
        </header>
        <body>
            <div>
                <RegisterComponent />
            </div>
        </body>
      </div>
  );
  
}

export default Inscription;