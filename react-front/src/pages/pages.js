const pages = {
    router: {
        Inscription: 'Inscription',
        Home: 'Home',
        CardsBuy: 'CardsBuy',
        CardsSell: 'CardsSell',
        Play: 'Play',
        Chat: 'Chat'
    },
    title: {
        home: 'Home',
        register: 'Inscription',
        login: 'Connexion',
        buy: 'Buy',
        sell: 'Sell',
        play: 'Play',
        chat: 'Chat'
    }
}

export default pages;