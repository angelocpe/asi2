import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { HeaderComponent } from '../components/HeaderComponent';
import { CardGridComponent } from '../components/CardGridComponent';
import { ActionLabelComponent } from '../components/ActionLabelComponent';
import { CardViewComponent } from '../components/CardViewComponent';
import { selectCurrentUser, selectCurrentUserId } from '../core/selectors/user.selectors';
import { selectCardsToBuy, selectCurrentCardId } from '../core/selectors/card.selectors';
import CardService from '../services/card.service';
import environment from '../environment/environment';
import { setCardsToBuy, setCurrentCard, setCurrentCardId } from '../core/actions/card.actions';
import StoreOrderDTO from '../DTOs/StoreOrderDTO';
import StoreService from '../services/store.service';
import UserService from '../services/user.service';
import { setCurrentUser } from '../core/actions/user.actions';


function CardsBuy() {

    const dispatch = useDispatch();
    const currentUser = useSelector(selectCurrentUser);
    const cardsToBuy = useSelector(selectCardsToBuy);
    const currentUserId = useSelector(selectCurrentUserId);
    const currentCardId = useSelector(selectCurrentCardId);

    const buyCard = async () => {
        const storeOrderDTO = new StoreOrderDTO({user_id: currentUserId, card_id: currentCardId});
        const storeResponse = await StoreService.postBuyStoreOrder(storeOrderDTO);
        if(storeResponse) {
            alert('Achat effectué !');
            dispatch(setCurrentCard(null));
            dispatch(setCurrentCardId(null));

            const user = await UserService.getUserById(currentUserId);
            if(user) {
                dispatch(setCurrentUser(user));
            }

        } else {
            alert('Erreur lors de l\'achat :(');
        }
    }

    async function fetchCardsToBuy(){
        const cards = await CardService.getAllCardsToSell();
        if(cards) {
            dispatch(setCardsToBuy(cards));
        } else {
            dispatch(setCardsToBuy([]));
        }
    }

    useEffect(() => {
        dispatch(setCurrentCard(null));
        setInterval(fetchCardsToBuy, environment.api_set_interval_time);
    }, []);

    return (
        <div className="App">
            <header className="App-header">
                {(() => { 
                    if (!currentUser) {
                        return (
                            <HeaderComponent account="" surname= ""/>
                        )
                    } else {
                        return (
                            <HeaderComponent account={currentUser.account} surname={currentUser.login}/>
                        )
                    }
                })()}
            </header>
            <body>
                <div>
                    <ActionLabelComponent title="Buy" />
                    <CardGridComponent cards={cardsToBuy}/>
                    <CardViewComponent text="Acheter" actionFunction={buyCard}/>
                </div>
            </body>
        </div>
    );
}

export default CardsBuy;