import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { HeaderComponent } from '../components/HeaderComponent';
import { CardGridComponent } from '../components/CardGridComponent';
import { ActionLabelComponent } from '../components/ActionLabelComponent';
import { CardViewComponent } from '../components/CardViewComponent';
import { selectCurrentUser, selectCurrentUserId } from '../core/selectors/user.selectors';
import { selectCardsToBuy, selectCurrentCardId, selectUserCards } from '../core/selectors/card.selectors';
import CardService from '../services/card.service';
import environment from '../environment/environment';
import { setCardsToBuy, setCurrentCard, setCurrentCardId, setUserCards } from '../core/actions/card.actions';
import StoreOrderDTO from '../DTOs/StoreOrderDTO';
import StoreService from '../services/store.service';
import UserService from '../services/user.service';
import { setCurrentUser } from '../core/actions/user.actions';


function CardsSell() {

    const dispatch = useDispatch();
    const currentUser = useSelector(selectCurrentUser);
    const cardsToSell = useSelector(selectUserCards);
    const currentUserId = useSelector(selectCurrentUserId);
    const currentCardId = useSelector(selectCurrentCardId);

    const sellCard = async () => {
        const storeOrderDTO = new StoreOrderDTO({user_id: currentUserId, card_id: currentCardId});
        const storeResponse = await StoreService.postSellStoreOrder(storeOrderDTO);
        if(storeResponse) {
            alert('Vente effectué !');
            dispatch(setCurrentCard(null));
            dispatch(setCurrentCardId(null));

            const user = await UserService.getUserById(currentUserId);
            if(user) {
                dispatch(setCurrentUser(user));
            }

        } else {
            alert('Erreur lors de la vente :(');
        }
    }

    async function fetchUserCards(){
        const cards = await CardService.getUserCardsByUserId(currentUserId);
        if(cards) {
            dispatch(setUserCards(cards));
        }
    }

    useEffect(() => {
        dispatch(setCurrentCard(null));
        setInterval(fetchUserCards, environment.api_set_interval_time);
    }, []);

    return (
        <div className="App">
            <header className="App-header">
                {(() => { 
                    if (!currentUser) {
                        return (
                            <HeaderComponent account="" surname= ""/>
                        )
                    } else {
                        return (
                            <HeaderComponent account={currentUser.account} surname={currentUser.login}/>
                        )
                    }
                })()}
            </header>
            <body>
                <div>
                    <ActionLabelComponent title="Sell" />
                    <CardGridComponent cards={cardsToSell}/>
                    <CardViewComponent text="Vendre" actionFunction={sellCard}/>
                </div>
            </body>
        </div>
    );
}

export default CardsSell;