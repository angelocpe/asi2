import React from 'react';
import { HeaderComponent } from '../components/HeaderComponent';
import { MenuListComponent } from '../components/MenuListComponent';
import { LoginComponent } from '../components/LoginComponent';
import { selectCurrentUser } from '../core/selectors/user.selectors';
import { useSelector } from 'react-redux';

function Home() {
  let connexion = false;

  const currentUser = useSelector(selectCurrentUser);

  if(currentUser) {
    connexion = true;
  } else{
    connexion = false; 
  }
  
  return (
    <div className="App">
      <header className="App-header" >
          {(() => { 
            if (!connexion) {
              return (
                <HeaderComponent account="" surname= ""/>
              )
            }else{
              return (
                <HeaderComponent account={currentUser.account} surname={currentUser.login}/>
              )
            }
          })()}
      </header>
      <body>
        <div> 
          {(() => {
            if (!connexion) {
              return (
                <LoginComponent/>
              )
            }else{
              return (
                <MenuListComponent/>
              )
            }
          })()}
          </div>
      </body>
    </div>
  );
}

export default Home;