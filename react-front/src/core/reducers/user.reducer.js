import UserActions from "../actions/user.actions";

export const initialState = {
    currentUserId: null,
    currentUser: null,
    otherUserId: null,
    otherUser: null,
    otherConnectedUsersIds: [],
    allOtherUsersIds: [],
    allUsers: [],
    allOtherUsers: []
}

/**
    Reducer
*/
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case UserActions.SET_CURRENT_USER_ID:
            return {
                ...state,
                currentUserId: action.payload.currentUserId,
            };
        
        case UserActions.SET_CURRENT_USER:
            return {
                ...state,
                currentUser: action.payload.currentUser,
            };
        
        case UserActions.SET_OTHER_USER_ID:
            return {
                ...state,
                otherUserId: action.payload.otherUserId,
            };
        
        case UserActions.SET_OTHER_USER:
            return {
                ...state,
                otherUser: action.payload.otherUser,
            };

        case UserActions.SET_CONNECTED_OTHER_USERS_IDS:
            return {
                ...state,
                otherConnectedUsersIds: action.payload.otherConnectedUsersIds,
            };

        case UserActions.SET_ALL_OTHER_USERS_IDS:
            return {
                ...state,
                allOtherUsersIds: action.payload.allOtherUsersIds,
            };
        case UserActions.SET_ALL_USERS:
            let allOtherUsers = []
            if(state.currentUserId) {
                for(const user of action.payload.allUsers) {
                    if(user.id !== state.currentUserId) {
                        allOtherUsers.push(user);
                    }
                }
            }
            return {
                ...state,
                allUsers: action.payload.allUsers,
                allOtherUsers: allOtherUsers
            };
        default:
            return state
    }
}

export default userReducer