import CardActions from '../actions/card.actions';

const initialState = {
  cards: [],
  userCards: [],
  cardsToBuy: [],
  currentCard: null,
  currentCardId: null
}

/**
  Reducer
*/
const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case CardActions.SET_ALL_CARDS:
        return {
            ...state,
            cards: action.payload.cards,
        }
    case CardActions.SET_USER_CARDS:
        return {
            ...state,
            userCards: action.payload.userCards,
        }
    case CardActions.SET_CARDS_TO_BUY:
        return {
            ...state,
            cardsToBuy: action.payload.cardsToBuy,
        }
    case CardActions.SET_CURRENT_CARD:
        return {
            ...state,
            currentCard: action.payload.currentCard
        }
    case CardActions.SET_CURRENT_CARD_ID:
        return {
            ...state,
            currentCardId: action.payload.currentCardId
        }
    default:
      return state
  }
}

export default cardReducer
