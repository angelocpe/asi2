import UserActions from "../actions/user.actions";
import combinedReducer from "./combined.reducer";
import storageSession from 'redux-persist/lib/storage/session';

const rootReducer = (state, action) => {
    if(action.type === UserActions.SIGNOUT_REQUEST) {
        storageSession.removeItem('persist:root')
        return combinedReducer(undefined, action);
    }
    return combinedReducer(state, action);
}

export default rootReducer;