import ChatActions from '../actions/chat.actions';

const initialState = {
  currentChatMessages: []
}

/**
  Reducer
*/
const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case ChatActions.SET_CURRENT_MESSAGES:
      return {
        ...state,
        currentChatMessages: action.payload.currentMessages,
      }
    case ChatActions.ADD_ONE_MESSAGE:
      return {
        ...state,
        currentChatMessages: [...state.currentChatMessages, action.payload.message]
      }
    default:
      return state
  }
}

export default chatReducer
