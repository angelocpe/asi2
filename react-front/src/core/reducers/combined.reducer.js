import { combineReducers } from 'redux';
import chatReducer from './chat.reducer';
import userReducer from './user.reducer';
import cardReducer from './card.reducer';

const initialLogState = {};

const logReducer = (state = initialLogState, action) => {
    console.log(action.type, action.payload);
    return state;
}

const combinedReducer = combineReducers({
    chatState: chatReducer,
    userState: userReducer,
    cardState: cardReducer,
    logReducer: logReducer
});

export default combinedReducer;