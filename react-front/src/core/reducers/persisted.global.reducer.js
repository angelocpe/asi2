import rootReducer from './root.reducer';
import { persistReducer } from 'redux-persist';
import storageSession from 'redux-persist/lib/storage/session';

const persistConfig = {
  key: 'root',
  storage: storageSession,
};

const persistedGlobalReducer = persistReducer(persistConfig, rootReducer)

export default persistedGlobalReducer;