/*
    ActionsTypes
*/

const UserActions = {
    SET_CURRENT_USER_ID: '@@user/SET_CURRENT_USER_ID',
    SET_CURRENT_USER: '@@user/SET_CURRENT_USER',
    SET_OTHER_USER_ID: '@@user/SET_OTHER_USER_ID',
    SET_OTHER_USER: '@@user/SET_OTHER_USER',
    SET_CONNECTED_OTHER_USERS_IDS: '@@user/SET_CONNECTED_OTHER_USERS_IDS',
    SET_ALL_OTHER_USERS_IDS: '@@user/SET_ALL_OTHER_USERS_IDS',
    SET_ALL_USERS: '@@user/SET_ALL_USERS',
    SIGNOUT_REQUEST: '@@user/SIGNOUT_REQUEST',
}

/*
    Actions creators
*/
  
export function setCurrentUserId(currentUserId) {
    return { type: UserActions.SET_CURRENT_USER_ID, payload: { currentUserId }};
};

export function setCurrentUser(currentUser) {
    return { type: UserActions.SET_CURRENT_USER, payload: { currentUser } };
};

export function setOtherUserId(otherUserId) {
    return { type: UserActions.SET_OTHER_USER_ID, payload: { otherUserId }};
};
  
export function setOtherUser(otherUser) {
    return { type: UserActions.SET_OTHER_USER, payload: { otherUser } };
};
  
export function setOtherConnectedUsersIds(otherConnectedUsersIds) {
    return { type: UserActions.SET_CONNECTED_OTHER_USERS_IDS, payload: { otherConnectedUsersIds }};
};
  
export function setAllOtherUsersIds(allOtherUsersIds) {
    return { type: UserActions.SET_ALL_OTHER_USERS_IDS, payload: { allOtherUsersIds }};
};

export function setAllUsers(allUsers) {
    return { type: UserActions.SET_ALL_USERS, payload: { allUsers }};
};

export function signout() {
    return { type: UserActions.SIGNOUT_REQUEST, payload: { } };
};

export default UserActions;