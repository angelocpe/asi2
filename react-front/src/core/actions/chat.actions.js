/**
    ActionsTypes
*/

 const ChatActions = {
    SET_CURRENT_MESSAGES: '@@chat/SET_CURRENT_MESSAGES',
    ADD_ONE_MESSAGE: '@@chat/ADD_ONE_MESSAGE',
}

/**
    Actions creators
*/

export function setCurrentMessages(currentMessages){
    return { type: ChatActions.SET_CURRENT_MESSAGES, payload: { currentMessages } };
}

export function addOneMessage(message) {
    return { type: ChatActions.ADD_ONE_MESSAGE, payload: { message } };
};

export default ChatActions