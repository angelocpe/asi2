/**
    ActionsTypes
*/

const CardActions = {
    SET_ALL_CARDS: '@@card/SET_ALL_CARDS',
    SET_USER_CARDS: '@@card/SET_USER_CARDS',
    SET_CARDS_TO_BUY: '@@card/SET_CARDS_TO_BUY',
    SET_CURRENT_CARD: '@@card/SET_CURRENT_CARD',
    SET_CURRENT_CARD_ID: '@@card/SET_CURRENT_CARD_ID',
}

/**
    Actions creators
*/

export function setCards(cards){
    return { type: CardActions.SET_ALL_CARDS, payload: { cards } };
}

export function setUserCards(userCards){
    return { type: CardActions.SET_USER_CARDS, payload: { userCards } };
}

export function setCardsToBuy(cardsToBuy){
    return { type: CardActions.SET_CARDS_TO_BUY, payload: { cardsToBuy } };
}

export function setCurrentCard(currentCard) {
    return { type: CardActions.SET_CURRENT_CARD, payload: { currentCard } };
};

export function setCurrentCardId(currentCardId) {
    return { type: CardActions.SET_CURRENT_CARD_ID, payload: { currentCardId } };
};

export default CardActions