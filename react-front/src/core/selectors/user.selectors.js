export const selectCurrentUserId = state => state.userState.currentUserId
export const selectCurrentUser = state => state.userState.currentUser
export const selectOtherUserId = state => state.userState.otherUserId
export const selectOtherUser = state => state.userState.otherUser
export const selectOtherConnectedUsersIds = state => state.userState.otherConnectedUsersIds
export const selectAllOtherUsersIds = state => state.userState.allOtherUsersIds
export const selectAllUsers = state => state.userState.allUsers
export const selectAllOtherUsers = state => state.userState.allOtherUsers