export const selectCards = state => state.cardState.cards
export const selectUserCards = state => state.cardState.userCards
export const selectCardsToBuy = state => state.cardState.cardsToBuy
export const selectCurrentCard = state => state.cardState.currentCard
export const selectCurrentCardId = state => state.cardState.currentCardId