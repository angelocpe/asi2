import React from "react";
import io from "socket.io-client";
import environment from "../../environment/environment";

export const socket = io(environment.chat_socket_url, { transports: ["websocket"] });
export const SocketContext = React.createContext();

const SocketEvents = {
    login_in: 'login_in',
    login_out: 'login_out',
    logged_in_users_ids_out: 'logged_in_users_ids_out',
    all_users_ids_out: "all_users_ids_out",
    new_chat_out: "new_chat_out",
    send_message_in: "send_message_in",
    send_message_out: "send_message_out"
}

export default SocketEvents
