import { createStore } from 'redux';
import persistedGlobalReducer from '../reducers/persisted.global.reducer';

const store = createStore(persistedGlobalReducer);

export default store;