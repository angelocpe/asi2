import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import './App.css';
import Home from './pages/Home'
import Inscription from './pages/Inscription'
import CardsBuy from './pages/CardsBuy';
import CardsSell from './pages/CardsSell';
import { socket, SocketContext } from './core/socket/socket';
import store from './core/store/store';
import persistor from './core/store/persistor'
import { PersistGate } from 'redux-persist/integration/react';
import SocketService from './services/socket.service';
import pages from './pages/pages'
import ChatHome from './components/chat/ChatHome';

function App() {

  return (
    <SocketContext.Provider value={socket}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <BrowserRouter>
              <Routes>
                <Route path='/'>
                    <Route index element={<Home/>} />
                    <Route path={pages.router.Inscription} element={<Inscription/>} />
                    <Route path={pages.router.CardsBuy} element={<CardsBuy/>} />
                    <Route path={pages.router.CardsSell} element={<CardsSell/>} />
                    <Route path={pages.router.Home} element={<Home/>} />
                    <Route path={pages.router.Chat} element={<ChatHome/>} />
                </Route>
              </Routes>
          </BrowserRouter>
        </PersistGate>
      </Provider>
    </SocketContext.Provider>
  );
}

export default App;
