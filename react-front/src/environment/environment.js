const environment = {
    // changer localhost:808X par https://asi2-backend-market.herokuapp.com 
    // pour tester sans les microservices
    api_urls: {
        card: 'http://localhost:8080',
        user: 'http://localhost:8081',
        store: 'http://localhost:8082',
        messages: 'http://localhost:8084'
    },
    chat_socket_url: 'http://localhost:8083',
    api_set_interval_time: 3000,
}

export default environment;