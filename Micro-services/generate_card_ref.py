import random

names = [
    'Armamite',
    'Girafree',
    'Weasou',
    'Falcyss',
    'Quilphin',
    'Iromadillo',
    'Starium',
    'Bonabura',
    'Magmeleon',
    'Thortoise',
    'Jaguaphy',
    'Chamesion',
    'Kangararing',
    'Weasithe',
    'Kinebug',
    'Electribug',
    'Elecela',
    'Ramatross',
    'Tortoros',
    'Mohawk',
    'Buffasion',
    'Kangamire',
    'Scorpung',
    'Komodory',
    'Eletopus',
    'Boneroach',
    'Elecingo',
    'Gothacle',
    'Pyrose',
    'Porcupike',
    'Jaguafree',
    'Gazetuff',
    'Kangarithe',
    'Buffix',
    'Electrimadillo',
    'Fluhog',
    'Stunatee',
    'Fiereen',
    'Pelicanine',
    'Salamaniac',
    'Wolvezel',
    'Flamipip',
    'Lobstaring',
    'Termix',
    'Fiepion',
    'Venodine',
    'Venomora',
    'Belloyote',
    'Ottermite',
    'Mohawk',
    'Goriceon',
    'Gazectric',
    'Pandeat',
    'Cobraring',
    'Chillray',
    'Skeletile',
    'Earthoth',
    'Ashossum',
    'Autoad',
    'Caterpixie',
    'Anteron',
    'Alligabyss',
    'Leopepi',
    'Scorpygon',
    'Golemingo',
    'Glacimadillo',
    'Bonadillo',
    'Terrium',
    'Ursign',
    'Hamstun',
    'Dradrill',
    'Drapix',
    'Vulturuno',
    'Chickepi',
    'Whirlwhale',
    'Venotile',
    'Whirluar',
    'Voltos',
    'Frostrich',
    'Tomatoad',
    'Wallarak',
    'Kangaron',
    'Termuzz',
    'Pengee',
    'Fluling',
    'Crazewing',
    'Elecossum',
    'Speceon',
    'Rabbite',
    'Mascotton',
    'Rhinoly',
    'Gorimish',
    'Komoduna',
    'Crocama',
    'Blastfly',
    'Elewhale',
    'Gothowary',
    'Melteton',
    'Pandarkness',
    'Lobsteroid'
]

families = [
    'chien',
    'chat',
    'pinguoin',
    'pigeon',
    'lezard'
]
affinities = [
    'eau',
    'feu',
    'plante',
    'electrique',
    'acier',
    'dragon',
    'spectre'
]

imgUrlStart = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'
smallImgUrl = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/'
imgUrlEnd = '.png'

script = ''
statementStart = 'INSERT INTO CARD_REFERENCE (name, description, family, affinity, img_url, small_img_url) VALUES '
statements = statementStart
modulo = 25
for i in range(1, 101):
    family = random.choice(families)
    affinity = random.choice(affinities)
    description = "Pokemon de famille {} et d\'\'affinite {}".format(family, affinity)
    if(i % modulo == 0):
        statements = statements + '(\'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\');'.format(
            names[i-1],
            description,
            family,
            affinity,
            imgUrlStart + str(i) + imgUrlEnd,
            smallImgUrl + str(i) + imgUrlEnd,
        )
        script = script + statements + '\n\n'
        statements = statementStart
    else:
        statements = statements + '\n\t'
        statements = statements + '(\'{}\', \'{}\', \'{}\', \'{}\', \'{}\', \'{}\'),'.format(
            names[i-1],
            description,
            family,
            affinity,
            imgUrlStart + str(i) + imgUrlEnd,
            smallImgUrl + str(i) + imgUrlEnd
        )

f = open("data.sql", "w")
f.write(script)
f.close()