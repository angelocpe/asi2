package com.cpe.springboot.esb_public;

import java.util.List;

public class BasicMessage<T, E> {
    private E actionType;
    private String callbackUrl;
    private List<T> objectsDTOs;

    public BasicMessage() {

    }

    public BasicMessage(E actionType, String callbackUrl, List<T> objectsDTOs) {
        this.actionType = actionType;
        this.callbackUrl = callbackUrl;
        this.objectsDTOs = objectsDTOs;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public List<T> getObjectsDTOs() {
        return objectsDTOs;
    }

    public void setObjectsDTOs(List<T> objectsDTOs) {
        this.objectsDTOs = objectsDTOs;
    }

    public E getActionType() {
        return actionType;
    }

    public void setActionType(E actionType) {
        this.actionType = actionType;
    }

    @Override
    public String toString() {
        return "BasicMessage{" +
                "callback='" + callbackUrl + '\'' +
                ", objectDTO=" + objectsDTOs +
                ", actionType=" + actionType +
                '}';
    }
}
