package com.cpe.springboot.esb_public;

public interface ReceiverInterface<T, U> {
    public void receiveMessage(BasicMessage<T, U> message);
}
