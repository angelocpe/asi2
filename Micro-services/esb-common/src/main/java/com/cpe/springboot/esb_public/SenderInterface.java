package com.cpe.springboot.esb_public;

public interface SenderInterface<T, U> {
    public void sendMessage(BasicMessage<T, U> message);
}
