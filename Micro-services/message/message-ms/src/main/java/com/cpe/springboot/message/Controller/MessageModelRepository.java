package com.cpe.springboot.message.Controller;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.message.model.MessageModel;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageModelRepository extends CrudRepository<MessageModel, Integer>{

    @Query("SELECT m FROM MessageModel m WHERE (m.senderId = :user1Id and m.receiverId = :user2Id) or (m.senderId = :user2Id and m.receiverId = :user1Id) ORDER BY m.timestamp")
    List<MessageModel> findUsersMessagesByUsersIds(
        @Param("user1Id") Integer user1Id,
        @Param("user2Id") Integer user2Id
    );

    @Query("SELECT m FROM MessageModel m WHERE m.receiverId = null ORDER BY m.timestamp")
    List<MessageModel> findGlobalMessages();
}