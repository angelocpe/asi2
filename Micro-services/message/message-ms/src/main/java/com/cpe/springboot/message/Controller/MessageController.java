package com.cpe.springboot.message.Controller;

import com.cpe.springboot.message.tools.DTOMapper;
import com.cpe.springboot.micro_public.MessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class MessageController {

    private final MessageModelService messageModelService;

    @Autowired
    public MessageController(MessageModelService messageModelService) {
        this.messageModelService = messageModelService;
    }

    @GetMapping("messages")
    public List<MessageDTO> getUsersMessagesByUsersIds(
            @RequestParam(name = "user1id") Integer user1id,
            @RequestParam(name = "user2id") Integer user2id
    ) {
        return messageModelService.getUsersMessagesByUsersIds(user1id, user2id).stream().map(
                DTOMapper::fromMessageModelToMessageDto
        ).collect(Collectors.toList());
    }

    @GetMapping("globalmessages")
    public List<MessageDTO> getUsersMessagesByUsersIds() {
        return messageModelService.getGlobalChatMessages().stream().map(
                DTOMapper::fromMessageModelToMessageDto
        ).collect(Collectors.toList());
    }
}
