package com.cpe.springboot.message.tools;

import com.cpe.springboot.message.model.MessageModel;
import com.cpe.springboot.micro_public.MessageDTO;

public class DTOMapper {
	
	public static MessageDTO fromMessageModelToMessageDto(MessageModel messageModel) {
		MessageDTO messageDTO = new MessageDTO(
				messageModel.getMessageId(),
				messageModel.getSenderId(),
				messageModel.getReceiverId(),
				messageModel.getMessage(),
				messageModel.getTimestamp()
		);
		return messageDTO;
	}
	
	public static MessageModel fromMessageDtoToMessageModel(MessageDTO messageDTO) {
		MessageModel messageModel = new MessageModel(
				messageDTO.getMessage_id(),
				messageDTO.getSender_id(),
				messageDTO.getReceiver_id(),
				messageDTO.getMessage(),
				messageDTO.getTimestamp()
		);
		return messageModel;
	}
	
}
