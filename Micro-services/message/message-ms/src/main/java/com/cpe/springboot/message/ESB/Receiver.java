package com.cpe.springboot.message.ESB;

import com.cpe.springboot.message.Controller.MessageModelService;
import com.cpe.springboot.message.tools.DTOMapper;
import com.cpe.springboot.micro_public.MessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class Receiver {

    private final MessageModelService messageModelService;

    @Autowired
    public Receiver(MessageModelService messageModelService) {
        this.messageModelService = messageModelService;
    }

    @JmsListener(destination = "${message.esb.queue.name}")
    public void receiveMessage(MessageDTO messageDTO) {
        System.out.println(messageDTO);
        messageModelService.sendMessage(DTOMapper.fromMessageDtoToMessageModel(messageDTO));
    }
}

