package com.cpe.springboot.message.Controller;

import com.cpe.springboot.message.model.MessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageModelService {
    private final MessageModelRepository messageRepository;

    @Autowired
    public MessageModelService(MessageModelRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<MessageModel> getUsersMessagesByUsersIds(Integer user1Id, Integer user2Id) {
        List<MessageModel> messageList = messageRepository.findUsersMessagesByUsersIds(user1Id, user2Id);
        return messageList;
    }

    public List<MessageModel> getGlobalChatMessages() {
        List<MessageModel> messageList = messageRepository.findGlobalMessages();
        return messageList;
    }
    public MessageModel sendMessage (MessageModel message){
        return messageRepository.save(message);
    }
}