package com.cpe.springboot.micro_public;

public class MessageDTO {
        private Integer message_id;

        private Integer sender_id;

        private Integer receiver_id;

        private String message;

        private long timestamp;

        public MessageDTO(Integer message_id, Integer sender_id, Integer receiver_id, String message, long timestamp) {
            this.message_id = message_id;
            this.sender_id = sender_id;
            this.receiver_id = receiver_id;
            this.message = message;
            this.timestamp = timestamp;
        }

        public MessageDTO() {
        }

        public Integer getMessage_id() {
            return message_id;
        }

        public void setMessage_id(Integer message_id) {
            this.message_id = message_id;
        }

        public Integer getSender_id() {
            return sender_id;
        }

        public void setSender_id(Integer sender_id) {
            this.sender_id = sender_id;
        }

        public Integer getReceiver_id() {
            return receiver_id;
        }

        public void setReceiver_id(Integer receiver_id) {
            this.receiver_id = receiver_id;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

    @Override
    public String toString() {
        return "MessageDTO{" +
                "messageId=" + message_id +
                ", senderId=" + sender_id +
                ", receiverId=" + receiver_id +
                ", message='" + message + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
