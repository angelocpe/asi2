package com.cpe.springboot.ESB;

import com.cpe.springboot.ESB.service.ReceiverStoreService;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.ReceiverInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Receiver implements ReceiverInterface<StoreActionDTO, ActionType> {

    private final ReceiverStoreService<StoreActionDTO, ActionType> receiverStoreService;

    private final RestTemplate restTemplate;

    @Autowired
    Receiver(ReceiverStoreService<StoreActionDTO, ActionType> receiverStoreService, RestTemplate restTemplate) {
        this.receiverStoreService = receiverStoreService;
        this.restTemplate = restTemplate;
    }

    @JmsListener(destination = "${transaction.esb.queue.name}")
    @Override
    public void receiveMessage(BasicMessage<StoreActionDTO, ActionType> message) {
        switch(message.getActionType()) {
            case BUY:
                receiverStoreService.treatBuyMessage(message);
                break;
            case SELL:
                receiverStoreService.treatSellMessage(message);
                break;
        }
    }
}
