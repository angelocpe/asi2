package com.cpe.springboot.external.services;

import java.util.*;

import com.cpe.springboot.micro_public.CardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CardService {

	private final RestTemplate restTemplate;

	@Value("${api.url.card}")
	private String api_url_card;

	@Autowired
	public CardService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public List<CardDTO> getAllCards() {
		ResponseEntity<CardDTO[]> response =
				restTemplate.getForEntity(api_url_card + "/cards", CardDTO[].class);
		if(response.getStatusCode() == HttpStatus.OK) {
			return Arrays.asList(Objects.requireNonNull(response.getBody()));
		}
		return null;
	}

	public CardDTO getCard(Integer id) {
		ResponseEntity<CardDTO> response =
				restTemplate.getForEntity(api_url_card + "/card/" + id, CardDTO.class);
		if(response.getStatusCode() == HttpStatus.OK) {
			return response.getBody();
		}
		return null;
	}

	public Boolean updateCard(CardDTO card, String callback) {
		HttpEntity<CardDTO> request = new HttpEntity<CardDTO>(card);
		ResponseEntity<CardDTO> response = restTemplate.exchange(
				api_url_card + "/card/" + card.getId() + "?callback=" + callback,
				HttpMethod.PUT,
				request,
				CardDTO.class
		);
		return response.getStatusCode() == HttpStatus.OK;
	}

}
