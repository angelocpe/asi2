package com.cpe.springboot.external.services;

import java.util.*;

import com.cpe.springboot.user_public.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

	private final RestTemplate restTemplate;

	@Value("${api.url.user}")
	private String api_url_user;

	@Autowired
	public UserService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public List<UserDTO> getAllUsers() {
		ResponseEntity<UserDTO[]> response =
				restTemplate.getForEntity(api_url_user + "/users", UserDTO[].class);
		if(response.getStatusCode() == HttpStatus.OK) {
			return Arrays.asList(Objects.requireNonNull(response.getBody()));
		}
		return null;
	}

	public UserDTO getUser(Integer id) {
		ResponseEntity<UserDTO> response =
				restTemplate.getForEntity(api_url_user + "/user/" + id, UserDTO.class);
		if(response.getStatusCode() == HttpStatus.OK) {
			return response.getBody();
		}
		return null;
	}

	public Boolean updateUser(UserDTO user, String callback) {
		HttpEntity<UserDTO> request = new HttpEntity<UserDTO>(user);
		ResponseEntity<UserDTO> response = restTemplate.exchange(
				api_url_user + "/user/" + user.getId() + "?callback=" + callback,
				HttpMethod.PUT,
				request,
				UserDTO.class
		);
		return response.getStatusCode() == HttpStatus.OK;
	}

}
