package com.cpe.springboot.ESB.service;

import com.cpe.springboot.ESB.ActionType;
import com.cpe.springboot.ESB.StoreActionDTO;
import com.cpe.springboot.micro_public.CardDTO;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.external.services.CardService;
import com.cpe.springboot.external.services.UserService;
import com.cpe.springboot.user_public.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ReceiverStoreServiceBean implements ReceiverStoreService<StoreActionDTO, ActionType> {

    private final UserService userService;
    private final CardService cardService;

    private final RestTemplate restTemplate;

    @Autowired
    public ReceiverStoreServiceBean(UserService userService, CardService cardService, RestTemplate restTemplate) {
        this.userService = userService;
        this.cardService = cardService;
        this.restTemplate = restTemplate;
    }

    @Override
    public void treatBuyMessage(BasicMessage<StoreActionDTO, ActionType> message) {
        System.out.println("Treating buy message :\n" + message);
        for(StoreActionDTO storeActionDTO : message.getObjectsDTOs()) {
            UserDTO userDTO = userService.getUser(storeActionDTO.getUserId());
            CardDTO cardDTO = cardService.getCard(storeActionDTO.getCardId());
            cardDTO.setUserId(storeActionDTO.getUserId());
            cardService.updateCard(cardDTO, "http://localhost:8082/store/notify_user_done/" + storeActionDTO.getTransactionId());
            userDTO.setAccount(userDTO.getAccount() - cardDTO.getPrice());
            userService.updateUser(userDTO, "http://localhost:8082/store/notify_user_done/" + storeActionDTO.getTransactionId());
        }
        restTemplate.getForEntity(message.getCallbackUrl(), String.class);
        System.out.println("Treated buy message");
    }

    @Override
    public void treatSellMessage(BasicMessage<StoreActionDTO, ActionType> message) {
        System.out.println("Treating sell message :\n" + message);
        for(StoreActionDTO storeActionDTO: message.getObjectsDTOs()) {
            UserDTO userDTO = userService.getUser(storeActionDTO.getUserId());
            CardDTO cardDTO = cardService.getCard(storeActionDTO.getCardId());
            cardDTO.setUserId(null);
            userDTO.setAccount(userDTO.getAccount() + cardDTO.getPrice());
            userService.updateUser(userDTO, "http://localhost:8082/store/notify_user_done/" +  storeActionDTO.getTransactionId());
            cardService.updateCard(cardDTO, "http://localhost:8082/store/notify_user_done/" +  storeActionDTO.getTransactionId());
        }
        restTemplate.getForEntity(message.getCallbackUrl(), String.class);
        System.out.println("Treated sell message");
    }
}
