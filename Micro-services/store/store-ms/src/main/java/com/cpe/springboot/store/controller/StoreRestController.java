package com.cpe.springboot.store.controller;

import com.cpe.springboot.store.model.StoreTransaction;
import java.util.List;
import com.cpe.springboot.store_public.StoreOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
@RequestMapping(value="/store")
public class StoreRestController {

	private final StoreService storeService;

	@Autowired
	public StoreRestController(
			StoreService storeService
	) {
		this.storeService = storeService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/buy")
	private Integer buyCard(@RequestBody StoreOrder order) {
		String callback =  ServletUriComponentsBuilder.fromCurrentServletMapping().toUriString() + "/store/notify_user_buy/" + order.getUser_id();
		return this.storeService.buyCard(order.getUser_id(), order.getCard_id(), callback);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/sell")
	private Integer sellCard(@RequestBody StoreOrder order) {
		String callback =  ServletUriComponentsBuilder.fromCurrentServletMapping().toUriString() + "/store/notify_user_sell/" + order.getUser_id();
		return this.storeService.sellCard(order.getUser_id(), order.getCard_id(), callback);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/notify_card_done/{id}")
	private boolean getTransactionCardDone(@PathVariable Integer id) {
		return storeService.updateCardState(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/notify_user_done/{id}")
	private boolean getTransactionUserDone(@PathVariable Integer id) {
		return storeService.updateUserState(id);
	}


	@RequestMapping(method = RequestMethod.GET, value = "/transaction")
	private List<StoreTransaction> getAllTransactions() {
		return storeService.getAllTransactions();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/notify_user_buy/{id}")
	private void getNotifyUserBought(@PathVariable Integer id) {
		// TODO add link to notification ms
		System.out.println("Getting notification buying for user with id: " + id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/notify_user_sell/{id}")
	private void getNotifyUserSell(@PathVariable Integer id) {
		// TODO add link to notification ms
		System.out.println("Getting notification selling for user with id: " + id);
	}

}
