package com.cpe.springboot.ESB;

import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.SenderInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class Sender implements SenderInterface<StoreActionDTO, ActionType> {

    private final JmsTemplate jmsTemplate;

    @Autowired
    Sender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Value("${transaction.esb.queue.name}")
    private String queue;

    @Override
    public void sendMessage(BasicMessage<StoreActionDTO, ActionType> message) {
        System.out.println("Added message to queue :\n" + message);
        jmsTemplate.convertAndSend(queue, message);
    }
}
