package com.cpe.springboot.ESB;

public class StoreActionDTO {
    private Integer transactionId;
    private Integer userId;
    private Integer cardId;

    public StoreActionDTO() {

    }

    public StoreActionDTO(Integer transactionId, Integer userId, Integer cardId) {
        this.transactionId = transactionId;
        this.userId = userId;
        this.cardId = cardId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    @Override
    public String toString() {
        return "StoreActionDTO{" +
                "transactionId=" + transactionId +
                ", userId=" + userId +
                ", cardId=" + cardId +
                '}';
    }
}
