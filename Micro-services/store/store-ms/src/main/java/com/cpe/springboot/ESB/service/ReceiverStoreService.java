package com.cpe.springboot.ESB.service;

import com.cpe.springboot.esb_public.BasicMessage;

public interface ReceiverStoreService<T, U> {
    public void treatBuyMessage(BasicMessage<T, U> message);
    public void treatSellMessage(BasicMessage<T, U> message);
}
