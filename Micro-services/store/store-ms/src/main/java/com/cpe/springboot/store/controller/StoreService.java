package com.cpe.springboot.store.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.cpe.springboot.ESB.ActionType;
import com.cpe.springboot.ESB.StoreActionDTO;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.SenderInterface;
import com.cpe.springboot.external.services.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cpe.springboot.store.model.StoreAction;
import com.cpe.springboot.store.model.StoreTransaction;
import com.cpe.springboot.external.services.UserService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StoreService {

	private final CardService cardService;
	private final UserService userService;
	private final StoreRepository storeRepository;

	private final SenderInterface<StoreActionDTO, ActionType> sender;

	@Autowired
	public StoreService(CardService cardService, UserService userService, StoreRepository storeRepository, SenderInterface<StoreActionDTO, ActionType> sender) {
		this.cardService = cardService;
		this.userService = userService;
		this.storeRepository = storeRepository;
		this.sender = sender;
	}

	public Integer buyCard(Integer user_id, Integer card_id, String callback) {
		StoreTransaction sT = storeRepository.save(new StoreTransaction(user_id, card_id, StoreAction.BUY));
		sender.sendMessage(
				new BasicMessage<StoreActionDTO, ActionType>(
					ActionType.BUY,
					callback,
					Collections.singletonList(
								new StoreActionDTO(
								sT.getId(),
								user_id,
								card_id
						)
					)
		));
		return sT.getId();
	}

	public Integer sellCard(Integer user_id, Integer card_id, String callback) {
		StoreTransaction sT = storeRepository.save(new StoreTransaction(user_id, card_id, StoreAction.SELL));
		sender.sendMessage(
				new BasicMessage<StoreActionDTO, ActionType>(
					ActionType.SELL,
					callback,
					Collections.singletonList(
							new StoreActionDTO(
									sT.getId(),
									user_id,
									card_id
							)
					)
		));
		return sT.getId();
	}


	// TODO make transactional
	// @Transactional(isolation = Isolation.SERIALIZABLE)
	public boolean updateCardState(Integer transactionId) {
		Optional<StoreTransaction> o_transaction = storeRepository.findById(transactionId);

		if(o_transaction.isPresent()) {
			StoreTransaction transaction = o_transaction.get();
			transaction.setCardState(true);
			transaction = storeRepository.save(transaction);
			notifyUserIfTransactionIsDone(transactionId);
			return true;
		}
		return false;
	}

	// TODO make transactional
	// @Transactional(isolation = Isolation.SERIALIZABLE)
	public boolean updateUserState(Integer transactionId) {
		Optional<StoreTransaction> o_transaction = storeRepository.findById(transactionId);

		if(o_transaction.isPresent()) {
			StoreTransaction transaction = o_transaction.get();
			transaction.setUserState(true);
			transaction = storeRepository.save(transaction);
			notifyUserIfTransactionIsDone(transactionId);
			return true;
		}
		return false;
	}

	public List<StoreTransaction> getAllTransactions() {
		List<StoreTransaction> sTList = new ArrayList<>();
		this.storeRepository.findAll().forEach(sTList::add);
		return sTList;

	}

	public boolean checkIfTransactionIsDone(StoreTransaction transaction) {
		return transaction.getCardState() && transaction.getUserState();
	}

	public void notifyUserIfTransactionIsDone(Integer id) {
		StoreTransaction transaction = storeRepository.findById(id).get();
		if(checkIfTransactionIsDone(transaction)) {
			// TODO add call to notify service when done
			System.out.println("Notifying user that transaction is done with userId : " + transaction.getUserId());
		}
	}

}
