package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.cpe.springboot.esb_public.BasicActionType;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.SenderInterface;
import com.cpe.springboot.external.services.CardService;
import com.cpe.springboot.user.ESB.Sender;
import com.cpe.springboot.user.tools.DTOMapper;
import com.cpe.springboot.user_public.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cpe.springboot.user.model.UserModel;

@Service
public class UserService {

	private final UserRepository userRepository;
	private final CardService cardService;

	private final SenderInterface<UserDTO, BasicActionType> sender;

	@Autowired
	public UserService(UserRepository userRepository, CardService cardService, SenderInterface<UserDTO, BasicActionType> sender) {
		this.userRepository = userRepository;
		this.cardService = cardService;
		this.sender = sender;
	}

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public UserModel getUser(String id) {
		Optional<UserModel> o_user = userRepository.findById(Integer.valueOf(id));

		if(o_user.isPresent()) {
			UserModel userModel = o_user.get();
			userModel.setCardList(cardService.getAllUserCards(userModel.getId()));
			return userModel;
		}

		return null;
	}

	public UserModel getUser(Integer id) {
		Optional<UserModel> o_user = userRepository.findById(Integer.valueOf(id));

		if(o_user.isPresent()) {
			UserModel userModel = o_user.get();
			userModel.setCardList(cardService.getAllUserCards(userModel.getId()));
			return userModel;
		}
		return null;
	}

	public UserDTO addUser(UserDTO user, String callback) {
		UserModel u = DTOMapper.fromUserDtoToUserModel(user);
		u = userRepository.save(u);
		cardService.getGenerateUserCards(u.getId(), callback + u.getId());
		return DTOMapper.fromUserModelToUserDTO(u);
	}

	public UserDTO updateUser(UserDTO user) {
		UserModel u = DTOMapper.fromUserDtoToUserModel(user);
		UserModel uBd =userRepository.save(u);
		return DTOMapper.fromUserModelToUserDTO(uBd);
	}

	public void sendUserUpdate(UserDTO userDTO, String callback) {
		sender.sendMessage(new BasicMessage<>(
				BasicActionType.UPDATE,
				callback,
				Collections.singletonList(userDTO)
		));
	}

	public UserDTO updateUser(UserModel user) {
		UserModel uBd = userRepository.save(user);
		return DTOMapper.fromUserModelToUserDTO(uBd);
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist = null;
		ulist = userRepository.findByLoginAndPwd(login, pwd);
		return ulist;
	}

}