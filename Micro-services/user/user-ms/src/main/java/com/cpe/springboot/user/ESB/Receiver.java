package com.cpe.springboot.user.ESB;
import com.cpe.springboot.esb_public.BasicActionType;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.ReceiverInterface;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user_public.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Receiver implements ReceiverInterface<UserDTO, BasicActionType> {

    private final UserService userService;

    private final RestTemplate restTemplate;

    @Autowired
    Receiver(UserService userService, RestTemplate restTemplate) {
        this.userService = userService;
        this.restTemplate = restTemplate;
    }

    @JmsListener(destination = "${user.esb.queue.name}")
    @Override
    public void receiveMessage(BasicMessage<UserDTO, BasicActionType> message) {

        switch(message.getActionType()) {
            case UPDATE:
                System.out.println("Treating update message :\n" + message);
                for(UserDTO u: message.getObjectsDTOs())
                    userService.updateUser(u);
                restTemplate.getForEntity(message.getCallbackUrl(), String.class);
                System.out.println("Treated update message :\n" + message);
                break;
        }
    }
}
