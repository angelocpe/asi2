package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;

import com.cpe.springboot.user.ESB.Sender;
import com.cpe.springboot.user.tools.DTOMapper;
import com.cpe.springboot.user_public.AuthDTO;
import com.cpe.springboot.user_public.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import com.cpe.springboot.user.model.UserModel;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {

	private final UserService userService;

	private final Sender sender;

	@Autowired
	public UserRestController(UserService userService, Sender sender) {
		this.userService=userService;
		this.sender = sender;
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users")
	private List<UserDTO> getAllUsers() {
		List<UserDTO> uDTOList=new ArrayList<UserDTO>();
		for(UserModel uM: userService.getAllUsers()){
			uDTOList.add(DTOMapper.fromUserModelToUserDTO(uM));
		}
		return uDTOList;

	}
	
	@RequestMapping(method=RequestMethod.GET,value="/user/{id}")
	private UserDTO getUser(@PathVariable Integer id) {
		UserModel userModel = userService.getUser(id);

		if(userModel != null) {
			return DTOMapper.fromUserModelToUserDTO(userModel);
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User id:"+id+", not found",null);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/user")
	public UserDTO addUser(@RequestBody UserDTO user) {
		String callback = ServletUriComponentsBuilder.fromCurrentContextPath().toUriString() + "/notify_user_cards_created/";
		return userService.addUser(user, callback);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}")
	public UserDTO updateUser(@RequestBody UserDTO user,@PathVariable Integer id, @RequestParam String callback) {
		user.setId(id);
		userService.sendUserUpdate(user, callback);
		return user;
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public void deleteUser(@PathVariable String id) {
		userService.deleteUser(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/auth")
	private Integer getAllCourses(@RequestBody AuthDTO authDto) {
		 List<UserModel> uList = userService.getUserByLoginPwd(authDto.getUsername(),authDto.getPassword());
		if( uList.size() > 0) {
			
			return uList.get(0).getId();
		}
		throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Authentification Failed",null);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/notify_user_cards_created/{id}")
	private void notifyUserCreatedCards(@PathVariable Integer id) {
		// TODO
		System.out.println("Get request for notifying user with id: " + id);
	}

}
