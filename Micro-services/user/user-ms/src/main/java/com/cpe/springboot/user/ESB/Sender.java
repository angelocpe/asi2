package com.cpe.springboot.user.ESB;

import com.cpe.springboot.esb_public.BasicActionType;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.SenderInterface;
import com.cpe.springboot.user_public.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class Sender implements SenderInterface<UserDTO, BasicActionType> {

    private final JmsTemplate jmsTemplate;

    @Autowired
    Sender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Value("${user.esb.queue.name}")
    private String queue;

    @Override
    public void sendMessage(BasicMessage<UserDTO, BasicActionType> message) {
        System.out.println("Added message to queue :\n" + message);
        jmsTemplate.convertAndSend(queue, message);
    }
}
