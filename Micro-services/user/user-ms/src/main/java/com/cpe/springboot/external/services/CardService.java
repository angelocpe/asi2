package com.cpe.springboot.external.services;

import com.cpe.springboot.micro_public.CardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class CardService {

	private final RestTemplate restTemplate;

	@Value("${api.url.card}")
	private String api_url_card;

	@Autowired
	public CardService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public Set<CardDTO> getAllUserCards(Integer id) {
		ResponseEntity<CardDTO[]> response =
				restTemplate.getForEntity(api_url_card + "/user_cards/" + id, CardDTO[].class);
		if(response.getStatusCode() == HttpStatus.OK) {
			Set<CardDTO> cardDTOS = new HashSet<CardDTO>();
			Collections.addAll(cardDTOS, response.getBody());
			return cardDTOS;
		}
		return null;
	}

	public Set<CardDTO> getGenerateUserCards(Integer id, String callback) {
		ResponseEntity<CardDTO[]> response =
				restTemplate.getForEntity(api_url_card + "/generate_user_cards/" + id + "?callback=" + callback, CardDTO[].class);
		if(response.getStatusCode() == HttpStatus.OK) {
			Set<CardDTO> cardDTOS = new HashSet<CardDTO>();
			Collections.addAll(cardDTOS, response.getBody());
			return cardDTOS;
		}
		return null;
	}


}
