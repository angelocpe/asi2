package com.cpe.springboot.user.tools;
import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user_public.UserDTO;

import java.util.stream.Collectors;

public class DTOMapper {
	
	public static UserDTO fromUserModelToUserDTO(UserModel uM) {
		UserDTO uDto =new UserDTO(
			uM.getId(),
			uM.getLogin(),
			uM.getPwd(),
			uM.getAccount(),
			uM.getLastName(),
			uM.getSurName(),
			uM.getEmail(),
			uM.getCardList()
		);
		return uDto;
	}
	
	public static UserModel fromUserDtoToUserModel(UserDTO uD) {
		UserModel uM =new UserModel(
			uD.getId(),
			uD.getLogin(),
			uD.getPwd(),
			uD.getAccount(),
			uD.getLastName(),
			uD.getSurName(),
			uD.getEmail(),
			uD.getCardList()
		);
		return uM;
	}
	
}
