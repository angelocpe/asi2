const listUsers = [];

class UserDao {
    constructor({}) {
        console.log(`new UserDao`);
    }

    getAllUsers() {
        return listUsers;
    }

    getUser(userId) {
        return listUsers.filter(
            u => u.id === Number(userId)
        )
        .pop();
    }
}

export default new UserDao({});