// Key = room_id, Value = owner_user_id
const created_rooms = new Map();

// Key = room_id, Value = [] (array of type Room)
// empty array in value is room log
const active_rooms = new Map();

// Variable changed when a new room is created
let ROOM_ID_INCREMENTOR = 0;

class RoomDao {
    constructor({}) {
        console.log(`new RoomDao`);
    }

    getCreatedRoomById(room_id) {
        return created_rooms.get(room_id);
    }

    addCreateRoom(owner_id) {
        ROOM_ID_INCREMENTOR += 1;
        created_rooms.set(ROOM_ID_INCREMENTOR, owner_id);
    }

    activateRoom(room_id, joining_user_id) {
        const owner_id = this.getCreatedRoomById(room_id);
        active_rooms.set(room_id, [owner_id, joining_user_id, []]);
    }

    joinCreatedRoom(room_id, joining_user_id) {
        active_rooms(room_id, joining_user_id);
        created_rooms.delete(room_id);
    }

    sendMessage(message) {
        const active_room = active_rooms.get(message.room_id);

        // Add message to active room
        active_room[2].push(message);

        active_rooms.set(message.room_id, active_room);
    }
}

export default new RoomDao({});