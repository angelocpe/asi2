// Key = user_id, Value = socket
const sockets = new Map();

class SocketDao {
    constructor({}) {
        console.log(`new SocketDao`);
    }

    getUserSocketByUserId(user_id) {
        return sockets.get(user_id);
    }

    // Return an array containing all users' sockets
    getAllSockets() {
        return sockets.values();
    }

    // Return an array containing other user's sockets
    getOthersSocket(current_user_id) {
        const othersSocket = new Map(sockets);
        othersSocket.delete(current_user_id);
        return othersSocket.values();
    }

    addUserSocket(socket, user_id) {
        sockets.set(user_id, socket);
    }

    removeUserSocket(user_id) {
        sockets.delete(user_id);
    }

    removeSocket(socket) {
        for(const [user_id, clientSocket] of sockets) {
            if(socket === clientSocket) {
                sockets.delete(user_id);
                break;
            }
        };
    }
}

export default new SocketDao({});