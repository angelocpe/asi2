import ChatService from "../services/ChatService.js";

class ChatSocketController {
    constructor({}) {
        console.log(`new ChatSocketController`);
    }

    send_message_in(socket, messageIn) {
        console.log('send_message_in', messageIn);
        ChatService.sendMessageIn(socket, messageIn);
    }
}

export default new ChatSocketController({});