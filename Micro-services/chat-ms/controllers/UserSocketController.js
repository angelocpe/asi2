import UserService from "../services/UserService.js";

class UserSocketController {
    constructor({}) {
        console.log(`new UserSocketController`);
    }

    getNewUserId() {
        return UserService.getNewUserId();
    }

    login_in(socket, loginIn) {
        console.log('user login', loginIn);
        UserService.loginUser(socket, loginIn);
    }

    logout_in(socket, logoutIn) {
        console.log('user logout', logoutIn);
        UserService.logoutUser(logoutIn);
    }

    removeSocket(socket) {
        UserService.removeSocket(socket);
    }
}

export default new UserSocketController({});