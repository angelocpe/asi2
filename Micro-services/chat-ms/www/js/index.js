const user_message_in = 'user_message_in';
const user_message_out = 'user_message_out';

var user_id = 0;

const socket = io();

const divMessages = document.getElementById('messages');

async function fetchData() {
    const resp = await fetch('http://localhost:8083/user');
      const user_data = await resp.json();
      user_id = user_data.user_id;
}

if(window.fetch) {
    fetchData();
}

function sendMessage() {
    const message = document.getElementById('message').value;
    if(message) {
        writeMessageOnSocket(user_id, message)
    } else {
        alert('Veuillez entrer un message');
    }
}

function writeMessageOnSocket(user_id, message) {
    socket.emit(user_message_in, {user_id: user_id, message: message});
}

socket.on(user_message_out, function(data){
    console.log(`received from user with id: ${data.user_id} message: \n${data.message}`);
    const divMessage = document.createElement('div');
    const pUserId = document.createElement('p');
    pUserId.innerText  = data.user_id;
    const pMessage = document.createElement('p');
    pMessage.innerText  = data.message;

    divMessage.appendChild(pUserId);
    divMessage.appendChild(pMessage);
    
    divMessages.appendChild(divMessage);
});

