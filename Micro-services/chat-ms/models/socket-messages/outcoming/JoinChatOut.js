class JoinChatOut {
    constructor({ chat_id, user_id }) {
        this.chat_id = chat_id;
        this.user_id = user_id;
    }
}

export default JoinChatOut;