class ChatMessage {
    constructor({ message_id, sender_id, receiver_id, message, timestamp }) {
        this.message_id = message_id;
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        this.message = message;
        this.timestamp = timestamp;
    }
}

export default ChatMessage;