import SocketDao from "../dao/SocketDao.js";
import CONFIG from '../config.json' assert { type: "json" };
import ChatMessage from "../models/ChatMessage.js";
import StompService from "./StompService.js";

class ChatService {
    constructor({}) {
        console.log(`new ChatService`);
    }

    // Send message to sender and receiver
    sendMessageIn(socket, messageIn) {
        const message = new ChatMessage(messageIn);
        
        // Send message to backend to save it
        StompService.sendMessage(message);

        // Send message to sender
        socket.emit(CONFIG.ChatSocketController.send_message_out, JSON.stringify(message));

        // Send message to receiver
        const otherUserSocket = SocketDao.getUserSocketByUserId(message.receiver_id);
        if(otherUserSocket) {
            otherUserSocket.emit(CONFIG.ChatSocketController.send_message_out, message);
        }
    }
}

export default new ChatService({});