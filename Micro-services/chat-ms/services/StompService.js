import stompit from 'stompit';

const connectOptions = {
    'host': 'localhost',
    'port': 61613
};

const headersSpring = {
    'destination': 'message-ms-in'
};

class StompService {
    constructor({}) {
        console.log(`new StompService`);
    }

    sendMessage(message) {
        stompit.connect(connectOptions, (error, clientStomp) => {
            if (error) {
                return console.error(error);
            }
            const frame = clientStomp.send(headersSpring);
            frame.write(Buffer.from(JSON.stringify(message)));
            frame.end();
            clientStomp.disconnect();
        });
    }

}

export default new StompService({});