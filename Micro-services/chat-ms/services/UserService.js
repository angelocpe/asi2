import SocketDao from "../dao/SocketDao.js";
import CONFIG from '../config.json' assert { type: "json" };
import http from "../premises/http.js";

let connectedUsers = new Set();

class UserService {
    constructor({}) {
        console.log(`new UserService`);
    }

    getAllusersIds() {
        return http.get_json(CONFIG.user_api_url)
            .then(users => {
                const usersIds = users.map(user => user.id);
                console.log('get users ids', usersIds);
                return usersIds;
            })
            .catch(error => {
                console.log('get users error', error);
                return [];
            });
    }

    async loginUser(socket, loginIn) {
        const allUsersIds = JSON.stringify(await this.getAllusersIds());

        // Add user socket to active sockets
        SocketDao.addUserSocket(socket, loginIn.user_id);

        // Add user to connected users
        connectedUsers.add(loginIn.user_id);
        
        // Send other connected users the new list of connected users
        // Send all users to connected users
        for(const curr_user_id of connectedUsers) {
            const userSocket = SocketDao.getUserSocketByUserId(curr_user_id);
            const otherUserIds = JSON.stringify([...connectedUsers].filter(userId => userId !== curr_user_id));
            userSocket.emit(CONFIG.UserSocketController.logged_in_users_ids_out, otherUserIds);
            userSocket.emit(CONFIG.UserSocketController.all_users_ids_out, allUsersIds);
        }
    }

    logoutUser(loginMessageIn) {
        SocketDao.removeUserSocket(loginMessageIn.user_id);
        connectedUsers.delete(loginMessageIn.user_id);
    }

    removeSocket(socket) {
        SocketDao.removeSocket(socket);
    }
}

export default new UserService({});