import express from 'express';
import * as http from 'http';
import { Server } from 'socket.io';
import cors from 'cors';

import CONFIG from './config.json' assert { type: "json" };
import UserSocketController from './controllers/UserSocketController.js';

import LoginIn from './models/socket-messages/incoming/LoginIn.js';
import LogoutIn from './models/socket-messages/incoming/LogoutIn.js';
import MessageIn from './models/socket-messages/incoming/MessageIn.js';
import ChatSocketController from './controllers/ChatSocketController.js';

const app = express();
app.use(cors());
const server = http.createServer(app);

const ioServer = new Server(server, {cors: {
    origin: '*'
}});

ioServer.on('connection', function(socket){

    // When a user logs in, add his socket to sockets
    socket.on(CONFIG.UserSocketController.login_in, function(data) {
        const loginIn = new LoginIn(JSON.parse(data));
        UserSocketController.login_in(socket, loginIn);
    });

    // When a user logs out, delete his socket from sockets
    socket.on(CONFIG.UserSocketController.logout_in, function(data) {
        const logoutIn = new LogoutIn(JSON.parse(data));
        UserSocketController.logout_in(socket, logoutIn);
    });

    // When a user sends a message
    socket.on(CONFIG.ChatSocketController.send_message_in, function(data) {
        const messageIn = new MessageIn(JSON.parse(data));
        ChatSocketController.send_message_in(socket, messageIn);
    });

    // When a socket disconnects
    socket.on('disconnect', function (data) {
        UserSocketController.removeSocket(socket);
    })
});

server.listen(CONFIG.port);