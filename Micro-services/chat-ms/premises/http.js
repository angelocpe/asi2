import fetch from "node-fetch";

class http {
    constructor({}) {
        console.log(`new http`);
    }

    // Sends a http get method to url and return json object
    get_json(api_url) {
        return new Promise((resolve, reject) => {
            return fetch(api_url)
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.json());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }

    // Sends a http post method to url and return json object
    post_json(api_url, data) {
        return new Promise((resolve, reject) => {
            return fetch(api_url, {
                    method: "POST",
                    headers: {'Content-Type': 'application/json'}, 
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.json());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }

    // Sends a http put method to url and return json object
    put_json(api_url, data) {
        return new Promise((resolve, reject) => {
            return fetch(api_url, {
                    method: "PUT",
                    headers: {'Content-Type': 'application/json'}, 
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.json());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }

    // Sends a http get method to url and return body
    get_body(api_url) {
        return new Promise((resolve, reject) => {
            return fetch(api_url)
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.body());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }

    // Sends a http post method to url and return body
    post_body(api_url, data) {
        return new Promise((resolve, reject) => {
            return fetch(api_url, {
                    method: "POST",
                    headers: {'Content-Type': 'application/json'}, 
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.body());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }

    // Sends a http put method to url and return body
    put_body(api_url, data) {
        return new Promise((resolve, reject) => {
            return fetch(api_url, {
                    method: "PUT",
                    headers: {'Content-Type': 'application/json'}, 
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (!response.ok) {
                        throw response.text().then(text => { throw new Error(text) });
                    }
                    return resolve(response.body());
                })
                .catch((error) => {
                    return reject(error);
                });
        });
    }
}

export default new http({});