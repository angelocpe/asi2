package com.cpe.springboot.card.tools;
import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.micro_public.CardDTO;

public class DTOMapper {
	
	public static CardDTO fromCardModelToCardDTO(CardModel cM) {
		CardDTO cDto =new CardDTO(
				cM,
				cM.getId(),
				cM.getEnergy(),
				cM.getHp(),
				cM.getDefence(),
				cM.getAttack(),
				cM.computePrice(),
				cM.getUserId(),
				cM.getStoreId()
		);
		return cDto;
	}
	
	public static CardModel fromCardDtoToCardModel(CardDTO cD) {
		CardModel cm=new CardModel(cD);
		cm.setEnergy(cD.getEnergy());
		cm.setHp(cD.getHp());
		cm.setDefence(cD.getDefence());
		cm.setAttack(cD.getAttack());
		cm.setPrice(cD.getPrice());
		cm.setId(cD.getId());
		cm.setUserId(cD.getUserId());
		cm.setStoreId(cD.getStoreId());
		return cm;
	}
	
}
