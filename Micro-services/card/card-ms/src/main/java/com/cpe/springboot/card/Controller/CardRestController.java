package com.cpe.springboot.card.Controller;

import java.util.*;

import com.cpe.springboot.micro_public.CardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.tools.DTOMapper;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController

public class CardRestController {

	private final CardModelService cardModelService;

	@Autowired
	public CardRestController(CardModelService cardModelService) {
		this.cardModelService=cardModelService;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/cards")
	private List<CardDTO> getAllCards() {
		List<CardDTO> cLightList=new ArrayList<>();
		for(CardModel c:cardModelService.getAllCardModel()){
			cLightList.add(DTOMapper.fromCardModelToCardDTO(c));
		}
		return cLightList;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/card/{id}")
	private CardDTO getCard(@PathVariable String id) {
		Optional<CardModel> rcard;
		rcard= cardModelService.getCard(Integer.valueOf(id));
		if(rcard.isPresent()) {
			return DTOMapper.fromCardModelToCardDTO(rcard.get());
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Card id:"+id+", not found",null);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/card")
	public CardDTO addCard(@RequestBody CardDTO card, @RequestParam String callback) {
		cardModelService.senderAddCard(card, callback);
		return null;
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/card/{id}")
	public CardDTO updateCard(@RequestBody CardDTO card, @PathVariable String id, @RequestParam String callback) {
		card.setId(Integer.valueOf(id));
		cardModelService.senderUpdateCard(card, callback);
	 	return null;
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/card/{id}")
	public void deleteCard(@PathVariable String id) {
		cardModelService.deleteCardModel(Integer.valueOf(id));
	}

	@RequestMapping(method=RequestMethod.GET, value="/cards_to_sell")
	private List<CardDTO> getCardsToSell() {
		List<CardDTO> list=new ArrayList<>();
		for( CardModel c : cardModelService.getAllCardToSell()){
			CardDTO cLight= DTOMapper.fromCardModelToCardDTO(c);
			list.add(cLight);
		}
		return list;

	}

	@RequestMapping(method=RequestMethod.GET, value="/user_cards/{id}")
	private List<CardDTO> getUserCards(@PathVariable Integer id) {
		List<CardDTO> list=new ArrayList<>();
		for( CardModel c : cardModelService.getCardsByUserId(id)){
			CardDTO cLight= DTOMapper.fromCardModelToCardDTO(c);
			list.add(cLight);
		}
		return list;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/generate_user_cards/{id}")
	private List<CardDTO> getGenerateUserCards(@PathVariable Integer id, @RequestParam String callback) {
		List<CardDTO> list=new ArrayList<>();
		for( CardModel c : cardModelService.getRandCard(5, id)){
			CardDTO cLight= DTOMapper.fromCardModelToCardDTO(c);
			list.add(cLight);
		}
		cardModelService.senderAddCards(list, callback);
		return list;
	}
}
