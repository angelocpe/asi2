package com.cpe.springboot.card.ESB;

import com.cpe.springboot.micro_public.CardDTO;
import com.cpe.springboot.esb_public.BasicActionType;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.SenderInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class Sender implements SenderInterface<CardDTO, BasicActionType> {

    private final JmsTemplate jmsTemplate;

    @Autowired
    Sender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Value("${card.esb.queue.name}")
    private String queue;

    @Override
    public void sendMessage(BasicMessage<CardDTO, BasicActionType> message) {
        System.out.println("Added message to queue :\n" + message);
        jmsTemplate.convertAndSend(queue, message);
    }
}
