package com.cpe.springboot.card.ESB;

import com.cpe.springboot.card.Controller.CardModelService;
import com.cpe.springboot.card.tools.DTOMapper;
import com.cpe.springboot.micro_public.CardDTO;
import com.cpe.springboot.esb_public.BasicActionType;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.ReceiverInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Receiver implements ReceiverInterface<CardDTO, BasicActionType> {
    private final CardModelService cardModelService;

    private final RestTemplate restTemplate;

    @Autowired
    Receiver(CardModelService cardModelService, RestTemplate restTemplate) {
        this.cardModelService = cardModelService;
        this.restTemplate = restTemplate;
    }

    @JmsListener(destination = "${card.esb.queue.name}")
    @Override
    public void receiveMessage(BasicMessage<CardDTO, BasicActionType> message) {

        switch(message.getActionType()) {
            case ADD:
                System.out.println("Treating add message :\n" + message);
                for(CardDTO cardDTO : message.getObjectsDTOs())
                    cardModelService.addCard(DTOMapper.fromCardDtoToCardModel(cardDTO));
                restTemplate.getForEntity(message.getCallbackUrl(), String.class);
                System.out.println("Treated add message");
                break;
            case UPDATE:
                for(CardDTO cardDTO : message.getObjectsDTOs())
                    cardModelService.updateCard(DTOMapper.fromCardDtoToCardModel(cardDTO));
                restTemplate.getForEntity(message.getCallbackUrl(), String.class);
                System.out.println("Treated update message :\n" + message);
                break;
        }
    }
}
