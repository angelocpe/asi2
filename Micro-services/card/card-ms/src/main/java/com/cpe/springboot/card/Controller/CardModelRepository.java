package com.cpe.springboot.card.Controller;

import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.card.model.CardModel;

import java.util.List;

public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    List<CardModel> findByUserId(Integer u);
}
