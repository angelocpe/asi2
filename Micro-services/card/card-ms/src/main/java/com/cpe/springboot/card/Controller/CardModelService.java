package com.cpe.springboot.card.Controller;

import java.util.*;

import com.cpe.springboot.micro_public.CardDTO;
import com.cpe.springboot.esb_public.BasicActionType;
import com.cpe.springboot.esb_public.BasicMessage;
import com.cpe.springboot.esb_public.SenderInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.card.model.CardReference;
import com.cpe.springboot.card.tools.DTOMapper;

@Service
public class CardModelService {
	private final CardModelRepository cardRepository;
	private final CardReferenceService cardRefService;

	private final SenderInterface<CardDTO, BasicActionType> sender;
	private Random rand;

	@Autowired
	public CardModelService(CardModelRepository cardRepository,CardReferenceService cardRefService, SenderInterface<CardDTO, BasicActionType> sender) {
		this.rand=new Random();
		this.cardRepository=cardRepository;
		this.cardRefService=cardRefService;
		this.sender = sender;
	}
	
	public List<CardModel> getAllCardModel() {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}

	public CardDTO addCard(CardModel cardModel) {
		CardModel cDb=cardRepository.save(cardModel);
		return DTOMapper.fromCardModelToCardDTO(cDb);
	}

	public void senderAddCard(CardDTO card, String callback) {
		BasicMessage<CardDTO, BasicActionType> message = new BasicMessage<>(
				BasicActionType.ADD,
				callback,
				Collections.singletonList(card)
		);
		sender.sendMessage(message);
	}

	public void senderAddCards(List<CardDTO> cards, String callback) {
		BasicMessage<CardDTO, BasicActionType> message = new BasicMessage<>(
				BasicActionType.ADD,
				callback,
				cards
		);
		sender.sendMessage(message);
	}

	public void senderUpdateCard(CardDTO card, String callback) {
		BasicMessage<CardDTO, BasicActionType> message = new BasicMessage<>(
				BasicActionType.UPDATE,
				callback,
				Collections.singletonList(card)
		);
		sender.sendMessage(message);
	}

	public void updateCardRef(CardModel cardModel) {
		cardRepository.save(cardModel);
	}
	public CardDTO updateCard(CardModel cardModel) {
		CardModel cDb=cardRepository.save(cardModel);
		return DTOMapper.fromCardModelToCardDTO(cDb);
	}
	public Optional<CardModel> getCard(Integer id) {
		return cardRepository.findById(id);
	}
	
	public void deleteCardModel(Integer id) {
		cardRepository.deleteById(id);
	}
	
	public List<CardModel> getRandCard(int nbr, Integer user_id){
		List<CardModel> cardList=new ArrayList<>();
		for(int i=0;i<nbr;i++) {
			CardReference currentCardRef=cardRefService.getRandCardRef();
			CardModel currentCard=new CardModel(currentCardRef);
			currentCard.setAttack(rand.nextFloat()*100);
			currentCard.setDefence(rand.nextFloat()*100);
			currentCard.setEnergy(100);
			currentCard.setHp(rand.nextFloat()*100);
			currentCard.setPrice(currentCard.computePrice());
			currentCard.setUserId(user_id);
			cardList.add(currentCard);
		}
		return cardList;
	}

	public List<CardModel> getCardsByUserId(Integer user_id) {
		return this.cardRepository.findByUserId(user_id);

	}
	public List<CardModel> getAllCardToSell(){
		return this.cardRepository.findByUserId(null);
	}
}

