# ASI2
## Dossier contenant le code des projects

Tout les différent projets sont dans le dossier Micro-services

## Explication des dossiers:

* **Micro-services**: Contient toutes les sources
* **react-front**: Application ReactJS contenant le code du front

Dans Micro-services
* **card**: Contient le project pour le micro-service et la partie exposé du micro-service card.
    * **card-ms**: Code Java du ms card
    * **card-public**: Code java de la partie exposé
* **user**: Similaire à card mais pour le ms user
* **store**: Similaire à card mais pour le ms store
* **esb-common**: généralisation du message, du receiver et du sender pour le traitement des messages envoyé sur l'esb de chaque ms
* **chat-app**: Application React pour créer une room de chat et discuter en privé avec quelqu'un
* **chat-ms**: ms chat fait avec NodeJS
* **ASI 2 Projet.postman_collection.json**: Fichier contenant les requêtes local pour tester les ms card, user et store

## Ce qui a été réalisé
* Tout l'atelier 1
* Implémentation de la généralisation de la réception et de l'envoi de message dans le bus dans esb-common dans le ms store
* Chat en temps réel connécté au ms-user mais pas de sauvegarde des chats une fois le process chat-ms tué

## Ce qu'il reste à faire
* Rendre transactionnal les requêtes d'achat et vente du store (TODO mais manque de temps)
* Ajouter l'historique des chats
* Ajouter le service de notifications (début d'esquisse dans chat-ms mais manque de temps)
* Créer des images docker pour avoir un proxy qui redirige sur chaque ms et sur un serveur qui hébérge le front
* CI/CD
